const addListener = (elements: NodeList | Element, listener: EventListener, eventType = 'click') => {
    if (elements instanceof NodeList) {
        elements.forEach(el => el.addEventListener(eventType, listener));
    }
    if (elements instanceof Element) {
        elements.addEventListener(eventType, listener);
    }
};

export default addListener;