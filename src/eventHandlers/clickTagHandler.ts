import renderMovies from '../renderers/renderMovies';
import { filmContainer } from '../index';
import fetchMovies from '../fetch/fetchMovies';
import createHtml from '../utils/createHtml';

const clickTagHandler = async (e: Event) => {
    const element = e.target as HTMLInputElement;
    const tag = element.id;
    const {movies, page} = await fetchMovies(tag);
    localStorage.setItem('page', `${page}`);
    localStorage.setItem('tag', tag);
    const html: string = createHtml(movies);
    await renderMovies(movies, html, filmContainer, true);
};

export default clickTagHandler
