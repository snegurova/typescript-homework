import { BASE_URL, API_KEY } from '../constants';
import { FetchData, MovieData } from '../types';

const fetchMovies = async (tag = 'popular', page = 1): Promise<FetchData> => {
    const url = `${BASE_URL}movie/${tag}?api_key=${API_KEY}&page=${page}`;
    const response = await fetch(url);
    const data = await response.json();
    console.log(data);
    return {
        movies: data.results,
        page: data.page,
        tag: tag
    };
};

export default fetchMovies;