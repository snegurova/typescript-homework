import { MovieData } from '../types';
import { BASE_IMAGE_URL } from '../constants';

const renderRandomMovie = async (movies: MovieData[]) => {
    const randomMovieIndex = Math.floor(Math.random() * 20);
    const randomMovie = movies[randomMovieIndex];
    const randomMovieContainer: HTMLElement | null = document.querySelector('#random-movie');
    const size = 'original';
    const imgUrl = `${BASE_IMAGE_URL}${size}/${randomMovie.backdrop_path}`;
    const html = `
        <div class="row py-lg-5">
                    <div
                        class="col-lg-6 col-md-8 mx-auto"
                        style="background: #2525254f"
                    >
                        <h1 id="random-movie-name" class="fw-light text-light">${randomMovie.title}</h1>
                        <p id="random-movie-description" class="lead text-white">
                            ${randomMovie.overview}
                        </p>
                    </div>
                </div>
    `;
    if (randomMovieContainer) {
        randomMovieContainer.setAttribute('style',
            `background-image: url("${imgUrl}");background-size: cover; background-position: center;`);
        randomMovieContainer.insertAdjacentHTML('beforeend', html);
    }
};

export default renderRandomMovie;