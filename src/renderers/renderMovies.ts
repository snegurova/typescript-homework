﻿import { MovieData } from '../types';

const renderMovies = (movies: MovieData[], html: string, filmContainer: Element | null,
                            emptyContainer = false) => {

    if (filmContainer) {
        if (emptyContainer) {
            filmContainer.innerHTML = '';
        }
        filmContainer.insertAdjacentHTML('beforeend', html);
    }
};

export default renderMovies;