import { flip } from '@popperjs/core';
import fetchMovies from './fetch/fetchMovies';
import createHtml from './utils/createHtml';
import addListener from './utils/addListener';
import clickTagHandler from './eventHandlers/clickTagHandler';
import renderMovies from './renderers/renderMovies';
import { MovieData } from './types'
import renderRandomMovie from './renderers/renderRandomMovie';
import clickLoadMoreHandler from './eventHandlers/clickLoadMoreHandler';

export const filmContainer = document.querySelector('#film-container');

export async function render(): Promise<void> {
    // TODO render your app here

    const {movies, page, tag} = await fetchMovies();
    localStorage.setItem('page', `${page}`);
    localStorage.setItem('tag', tag);
    await renderRandomMovie(movies);
    const html: string = createHtml(movies);
    await renderMovies(movies, html, filmContainer);

    const tagButtons = document.querySelectorAll('.btn-check');
    addListener(tagButtons, clickTagHandler, 'change');

    const loadMoreButton = document.querySelector("#load-more");
    if (loadMoreButton) {
        addListener(loadMoreButton, clickLoadMoreHandler);
    }


}
