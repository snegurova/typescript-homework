import renderMovies from '../renderers/renderMovies';
import { MovieData } from '../types';
import { filmContainer } from '../index';
import fetchMovies from '../fetch/fetchMovies';
import createHtml from '../utils/createHtml';

const clickLoadMoreHandler = async (e: Event) => {
    e.preventDefault();
    const pageString = localStorage.getItem('page');
    let page = 0;
    if (pageString) {
        page = Number.parseInt(pageString);
    }
    const tag = localStorage.getItem('tag');
    if (tag) {
        page += 1;
        const { movies } = await fetchMovies(tag, page);
        localStorage.setItem('page', `${page}`);
        const html: string = createHtml(movies);
        await renderMovies(movies, html, filmContainer);
    }
};

export default clickLoadMoreHandler
